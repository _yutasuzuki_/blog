import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import { unified } from 'unified'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype'
import rehypeHighlight from 'rehype-highlight'
import rehypeStringify from 'rehype-stringify'
import remarkGfm from 'remark-gfm'

const postsDirectory = path.join(process.cwd(), 'posts')

export function getSortedPostsData() {
  const fileNames = fs.readdirSync(postsDirectory)
  const allPostsData = fileNames.map(fileName => {
    const id = fileName.replace(/\.md$/, '')
    const fullPath = path.join(postsDirectory, fileName)
    const fileContents = fs.readFileSync(fullPath, 'utf8')
    const matterResult = matter(fileContents)
    return {
      id,
      ...matterResult.data
    }
  })
  return allPostsData.sort((a: any, b: any) => {
    if (a.created < b.created) {
      return 1
    } else {
      return -1
    }
  })
}

export function getAllPostIds() {
  const fileNames = fs.readdirSync(postsDirectory)
  return fileNames.map(fileName => {
    return {
      params: {
        id: fileName.replace(/\.md$/, ''),
      }
    }
  })
}

export async function getPostData(id) {
  const fullPath = path.join(postsDirectory, `${id}.md`)
  const fileContents = fs.readFileSync(fullPath, 'utf8')
  const matterResult = matter(fileContents)
  const processedContent = await unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(remarkRehype)
    .use(rehypeHighlight)
    .use(rehypeStringify)
    .process(matterResult.content)
  const contentHtml = processedContent.toString()

  return {
    id,
    contentHtml,
    ...matterResult.data
  }
}

// export async function getPostData(id) {
//   const fullPath = path.join(postsDirectory, `${id}.md`)
//   const fileContents = fs.readFileSync(fullPath, 'utf8')
//   const matterResult = matter(fileContents)
//   const processedContent = await unified()
//     .use(remarkParse)
//     .use(remarkGfm)
//     .use(remarkRehype)
//     .use(rehypeHighlight)
//     .use(rehypeStringify)
//     .process(matterResult.content)
//   const contentHtml = processedContent.toString()

//   return {
//     id,
//     contentHtml,
//     ...matterResult.data
//   }
// }

export function getPostsPageCount() {
  const fileNames = fs.readdirSync(postsDirectory)
  return Math.ceil(fileNames.length / 10)
}

export function getPagesCountData() {
  const counts = getPostsPageCount()
  const result = []
  for (let i = 1; i <= counts; i++) {
    result.push({
      params: {
        page: i.toString()
      }
    })
  }
  return result
}

export function getPostsData(currentPage: number) {
  const posts = getSortedPostsData()
  return posts.slice((currentPage - 1) * 10, currentPage * 10)
}

export function generateDescriptionFromHTML(text: string) {
  return text.replace(/<("[^"]*"|'[^']*'|[^'">])*>/g,'').slice(0, 130).replace(/\r?\n/g,"")
}