module.exports = {
  siteUrl: 'https://0ops.dev',
  generateRobotsTxt: true,
  sitemapSize: 7000,
  autoLastmod: false,
}