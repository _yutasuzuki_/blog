import styles from './FirstPost.module.css'
import Link from 'next/link'
import Image from 'next/image'
import { Date } from 'components/Date/Date'

export const FirstPost = ({ id, title, created, eyecatch }) => {
  return (
    <div className={styles.post}>
      <Link href={`/posts/${id}`}>
        <div className={styles.image}>
          <Image 
            alt={title}
            src={eyecatch}
            style={{ objectFit: 'contain' }}
            fill
            priority={true}
          />
        </div>
        <div className={styles.body}>
          <p className={styles.title}>
            {title}
          </p>
          <p className={styles.date}>
            <small>
              <Date dateString={created} />
            </small>
          </p>
        </div>
      </Link>
    </div>
  )
}
