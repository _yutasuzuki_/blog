import Head from 'next/head'
import styles from './Layout.module.css'
import Image from 'next/image'
import Link from 'next/link'
import type { ReactElement } from 'react'

type Props = {
  home?: boolean
  title?: string 
  description?: string
  ogImage?: string
  children: any
}

export const Layout: React.FC<Props> = ({ children, home, title = '', description = '', ogImage = 'https://0ops.dev/images/ogp.jpg' }) => {
  const desc = description.replace(/<("[^"]*"|'[^']*'|[^'">])*>/g,'').slice(0, 130).replace(/\r?\n/g,"")
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={desc} />
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:image" content={ogImage} />
        <meta name="og:title" content={title} />
        <meta name="twitter:title" content={title}/>
        <meta name="twitter:description" content={desc} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header className={styles.header}>
        <div className={styles.headerInner}>
          {home ? (
            <h1>
              <Link href="/" className={styles.logo}>
                <Image
                  src="/images/logo.png" 
                  style={{ 'objectFit': 'contain' }}
                  fill
                  sizes="82px"
                  alt="Oops!!" 
                />
              </Link>
            </h1>
          ) : (
            <Link href="/" className={styles.logo}>
              <Image
                src="/images/logo.png" 
                style={{ 'objectFit': 'contain' }}
                fill
                sizes="82px"
                alt="Oops!!" 
              />
            </Link>
          )}
          <p className={styles.headerText}>
            プログラミングとかのラフなブログ
          </p>
        </div>
      </header>
      <main className={styles.main}>{children}</main>
      <footer className={styles.footer}>
        <small>© yutasuzuki</small>
      </footer>
    </>
  )
}
