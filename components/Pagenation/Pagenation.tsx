import { useRouter } from 'next/router'
import styles from './Pagenation.module.css'
import CustomPagination from 'components/CustomPagenate/CustomPagenate'

export default function Pagenation({ currentPage, pageCount }) {
  const router = useRouter()

  const _handleOnChangePage = (e) => {
    router.push(`/posts/p/${e.selected + 1}`)
  }

  if (!pageCount) return null

  return (
    <div className={styles.paginate}>
      <CustomPagination
        previousLabel={"<"}
        nextLabel={">"}
        pageCount={pageCount}
        onPageChange={_handleOnChangePage}
        forcePage={currentPage - 1}
        // breakLabel={"..."}
        containerClassName={'pagination'}
        activeClassName={'active'}
        pageRangeDisplayed={10}
      />
    </div>
  )
}