import styles from './Posts.module.css'
import Link from 'next/link'
import Image from 'next/image'
import { Date } from 'components/Date/Date'
import { useMemo } from 'react'

export const Post = ({ id, title, created, eyecatch }) => {
  return (
    <li>
      <Link href={`/posts/${id}`} className={styles.post}>
        <div className={styles.image}>
          <Image 
            width={80}
            height={80}
            src={eyecatch}
            alt={title}
          />
        </div>
        <div className={styles.body}>
          <p>
            <span className={styles.title}>{title}</span>
          </p>
          <p className={styles.date}>
            <small>
              <Date dateString={created} />
            </small>
          </p>
        </div>
      </Link>
    </li>
  )
}

export const Posts = ({ posts }) => {
  const list = useMemo(() => {
    return posts.map((post) => {
      return (
        <Post key={post.id} {...post} />
      )
    })
  }, [posts])

  return (
    <ul className={styles.posts}>
      {list}
    </ul>
  )
}
