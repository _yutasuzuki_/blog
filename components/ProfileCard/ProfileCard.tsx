import { parseISO, format } from 'date-fns'
import styles from './ProfileCard.module.css'
import Link from 'next/link'
import { FaTwitter } from 'react-icons/fa'
import Image from 'next/image'

export const ProfileCard = () => {
  return (
    <div className={styles.container}>
      <div className={styles.thumbnail}>
        <Image
          src="/images/profile.jpg"
          alt="プロフィール画像"
          width={80}
          height={80}
        />
      </div>
      <div className={styles.content}>
        <p className={styles.name}>すずき ゆうた</p>
        <p className={styles.text}>
          愛知県でフリーランスのフロントエンド・エンジニアをしています。Reactを用いた開発が得意です。
          他にもプロジェクトマネジメントや組織マネジメントも行ってきました。エビデンスのない事でも自分の経験から書いていくので話半分くらいでお願いします。
        </p>
        <div className={styles.sns}>
          <a href="https://twitter.com/_yutasuzuki_" rel="noreferrer" target="_blank">
            <FaTwitter className={styles.twitter} />
          </a>
        </div>
      </div>
    </div>
  )
}

