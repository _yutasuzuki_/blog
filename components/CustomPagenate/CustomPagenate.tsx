import { useCallback, useMemo, useState } from "react";
import styles from "./CustomPagenate.module.css"

interface Props {
  previousLabel?: string;
  nextLabel?: string;
  pageCount: number;
  onPageChange: (...args: any) => void;
  pageRangeDisplayed?: number;
  breakLabel?: string;
  pageLinkClassName?: string;
  previousClassName?: string;
  nextClassName?: string;
  disabledClassName?: string;
  breakClassName?: string;
  containerClassName?: string;
  activeClassName?: string;
  gap?: Record<string, string>;
  forcePage?: number;
}

const CustomPagination = ({
  previousLabel = "Prev",
  nextLabel = "Next",
  pageCount,
  onPageChange,
  pageRangeDisplayed = 2,
  breakLabel = "...",
  pageLinkClassName,
  previousClassName,
  nextClassName,
  disabledClassName,
  breakClassName,
  containerClassName,
  activeClassName,
  forcePage,
}: Props) => {
  const [currentPage, setCurrentPage] = useState(forcePage || 0);

  const handlePageClick = useCallback(
    (page: number) => {
      onPageChange({ selected: page });
      setCurrentPage(page);
    },
    [onPageChange],
  );

  const pages = useMemo(() => {
    const pages = [];
    const leftSide = pageRangeDisplayed / 2;
    const rightSide = pageRangeDisplayed - leftSide;

    for (let i = 0; i < pageCount; i++) {
      const label = i + 1;
      let className = "";
      if (i === currentPage) className += styles.active;

      if (
        i === 0 ||
        i === pageCount - 1 ||
        (i >= currentPage - leftSide && i <= currentPage + rightSide)
      ) {
        pages.push(
          <li key={i}>
            <button onClick={() => handlePageClick(i)} className={className}>
              {label}
            </button>
          </li>,
        );
      } else if (
        (i < currentPage - leftSide && i === 1) ||
        (i > currentPage + rightSide && i === pageCount - 2)
      ) {
        pages.push(
          <li key={i}>
            <span className={breakClassName}>
              {breakLabel}
            </span>
          </li>,
        );
      }
    }

    return pages;
  }, [
    currentPage,
    pageCount,
    pageRangeDisplayed,
    pageLinkClassName,
    activeClassName,
    breakClassName,
    breakLabel,
  ]);

  return (
    <nav className={styles.pagination}>
      <button
        onClick={() => handlePageClick(currentPage - 1)}
        className={
          currentPage === 0 ? `${styles.previous} ${disabledClassName}` : styles.previous
        }
        disabled={currentPage === 0}
      >
        {previousLabel}
      </button>
      <ul>
        {pages}
      </ul>
      <button
        onClick={() => handlePageClick(currentPage + 1)}
        className={
          currentPage === pageCount - 1 ? `${styles.next} ${disabledClassName}` : styles.next
        }
        disabled={currentPage === pageCount - 1}
      >
        {nextLabel}
      </button>
    </nav>
  );
};

export default CustomPagination;