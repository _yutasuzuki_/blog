import Document, { Head, Html, Main, NextScript, DocumentContext } from 'next/document'

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    const isProduction = process.env.NODE_ENV === 'production';
    return { ...initialProps, isProduction }
  }

  render() {
    const { isProduction } = this.props as any
    return (
      <Html lang="ja">
        <Head>
          <meta name="google-site-verification" content="cqogh8z9kFXEk26rUYXAtq7o1fXNxB6Qg-cPXSI7RSM" />
          <link rel="stylesheet" href="/css/reset.css" />
          <link rel="stylesheet" href="/css/default.css" />
          <link rel="stylesheet" href="/css/atom-one-dark.min.css" />
          <link rel="stylesheet" href="/css/react-paginate.css" />
          {isProduction ? (
            <>
              <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154965844-2" />
              <script dangerouslySetInnerHTML={{
                __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-154965844-2');
                `
              }} />
            </>
          ): null}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}