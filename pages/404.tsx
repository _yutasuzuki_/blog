import React from 'react'
import { Layout } from 'components/Layout/Layout'

// production時(next buildの成果物を使っている時)のエラー表示に使われる
// See Also: https://nextjs.org/docs/advanced-features/custom-error-page

const ErrorPage = ({ text, stauts }) => (
  <Layout title={`ページが見つかりません | Oops!!`}>
    <div style={{ textAlign: 'center', marginTop: 180, marginBottom: 180 }}>
      <div style={{ margin: 'auto', position: 'relative', fontSize: 120, fontWeight: "bold", fontStyle: "italic" }}>
        404
      </div>
      <p style={{ fontSize: 24, color: '#101a33', marginTop: 24 }}>ページが見つかりません</p>
    </div>
  </Layout>
)

export default ErrorPage;