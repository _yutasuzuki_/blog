import Head from 'next/head'
import Link from 'next/link'
import { FirstPost } from 'components/FirstPost/FirstPost'
import { Posts } from 'components/Posts/Posts'
import { Layout } from 'components/Layout/Layout'
import { ProfileCard } from 'components/ProfileCard/ProfileCard'
import { getSortedPostsData } from 'lib/posts'
import { useMemo } from 'react'
import styles from './index.module.css'

export default function Page({ posts }) {

  const listItems = useMemo(() => {
    const [_, ...items] = posts
    return items
  }, [posts])

  return (
    <Layout
      home={true}
      title="Oops!! - プログラミングとかのラフなブログ"
      description={"愛知県でフリーランスで活動しているWebエンジニアのブログです。プログラミング以外にも何でもかける場所が欲しくて作りました。"}
    >
      <section className={styles.content}>
        <FirstPost {...posts[0]} />
        <Posts posts={listItems} />
        <div className={styles.btnMoreContainer}>
          <Link href="/posts/p/1" className={styles.btnMore}>投稿一覧</Link>
        </div>
      </section>
      <section className={styles.content}>
        <h2 className={styles.subTitle}>プロフィール</h2>
        <ProfileCard />
      </section>
    </Layout>
  )
}

export async function getStaticProps() {
  const posts = await getSortedPostsData()
  return {
    props: {
      posts: posts.splice(0, 5)
    }
  }
}
