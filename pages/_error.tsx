import React from 'react'
import { NextPage, NextPageContext } from 'next'
import Head from 'next/head'
import { Layout } from 'components/Layout/Layout'

// production時(next buildの成果物を使っている時)のエラー表示に使われる
// See Also: https://nextjs.org/docs/advanced-features/custom-error-page

const ErrorPage = ({ text, stauts }) => (
  <Layout title={`${stauts}エラー | Oops!!`}>
    <Head>
      <link rel="stylesheet" href="/css/react-paginate.css" />
    </Head>
    <div style={{ textAlign: 'center', marginTop: 180, marginBottom: 180 }}>
      <div style={{ fontSize: 120, fontFamily: 'Poppins,sans-serif', color: '#101a33' }}>{stauts}</div>
      <p style={{ fontSize: 24, color: '#101a33' }}>{text}</p>
    </div>
  </Layout>
)

interface Props {
  statusCode: number;
}

const Page: NextPage<Props> = ({ statusCode }) => {
  if (statusCode === 404) {
    return <ErrorPage text="ページが見つかりません" stauts={statusCode} />
  }
  if (statusCode === 200) {
    return <ErrorPage text="エラーが発生しました" stauts={statusCode} />
  }
  return <ErrorPage text="エラーが発生しました" stauts={statusCode} />
};

Page.getInitialProps = async ({ res, err }: NextPageContext) => {
  // statusCodeを算出する。
  // - resが存在する時はSSRであり、res.statusCodeをそのまま利用すれば良い。
  // - resがない場合はCSRである。
  //   - err.statusCodeがあればそれをそのまま利用する
  //   - 意図しない例外が起きてerrがここに渡ってくる場合、単なるErrorオブジェクトが入っていてstatusCodeプロパティがない。errがある時点でISEなので500にする
  // See Also: https://nextjs.org/docs/advanced-features/custom-error-page
  const statusCode = res ? res.statusCode : err ? err.statusCode ?? 500 : 404

  return { statusCode };
};

export default Page;