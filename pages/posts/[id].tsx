import Image from 'next/image'
import { useRouter } from 'next/router'
import { Layout } from 'components/Layout/Layout'
import { getAllPostIds, getPostData } from 'lib/posts'
import { Date } from 'components/Date/Date'
import { ProfileCard } from 'components/ProfileCard/ProfileCard'
import styles from './[id].module.css'

export default function Post({ postData }) {
  const router = useRouter()
  
  return (
    <Layout title={`${postData.title} | Oops!!`} description={postData.contentHtml} ogImage={postData.ogImage ? `https://0ops.dev${postData.ogImage}` : undefined}>
      <article className={styles.article}>
        <h1>{postData.title}</h1>
        <div>
          <Date dateString={postData.created} />
          {postData.updated && <Date dateString={postData.updated} />}
        </div>
        {postData.eyecatch && (
          <div className={styles.eyecatch}>
            <Image 
              fill
              alt={postData.title}
              src={postData.eyecatch}
              style={{ objectFit: 'contain' }}
              priority={true}
            />
          </div>
        )}
        <div className={styles.articleBody} dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
      </article>
      <div className={styles.profile}>
        <ProfileCard />
      </div>
      <div className={styles.navigation}>
        <a onClick={() => router.back()}>前のページに戻る</a>
      </div>
    </Layout>
  )
}

export async function getStaticPaths() {
  const paths = getAllPostIds()
  return {
    paths,
    fallback: false
  }
}

export async function getStaticProps({ params }) {
  const postData = await getPostData(params.id)
  return {
    props: {
      postData
    }
  }
}
