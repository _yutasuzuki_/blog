import { NextPageContext } from 'next'

export default function Page() {
  return <div />
}

export async function getServerSideProps(context: NextPageContext) {
  return {
    redirect: {
      permanent: false,
      destination: '/posts/p/1'
    }
  }
}