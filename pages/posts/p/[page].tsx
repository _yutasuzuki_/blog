import { Layout } from 'components/Layout/Layout'
import { Posts } from 'components/Posts/Posts'
import { getPostsData, getPostsPageCount, getPagesCountData } from 'lib/posts'
import dynamic from 'next/dynamic'

const Pagenation = dynamic(() => import('../../../components/Pagenation/Pagenation'), { ssr: false })

export default function Post({ currentPage, pageCount, posts }) {
  return (
    <Layout title={`投稿一覧(${currentPage}ページ目) | Oops!!`}>
      <Posts posts={posts} />
      <Pagenation
        currentPage={currentPage}
        pageCount={pageCount}
      />
    </Layout>
  )
}

export async function getStaticPaths() {
  const paths = getPagesCountData()
  return {
    paths,
    fallback: false
  }
}

export async function getStaticProps({ params }) {
  const posts = getPostsData(Number(params.page))
  const pageCount = getPostsPageCount()
  return {
    props: {
      currentPage: Number(params.page),
      pageCount,
      posts
    }
  }
}
