---
title: "有事マインドと平時マインド"
created: "2023-05-09"
eyecatch: "/images/emergency-and-peacetime/eyecatch.png"
ogImage: "/images/emergency-and-peacetime/eyecatch.png"
---

会社はフェーズによって有事にも平時にもなり、どちらのフェーズも会社が続いていけば起こりうる事だと思います。

その為、フェーズが有事だろうと平時であろうと**善悪の話ではない**ことを前提にしています。

## 自分の考える有事マインドと平時マインド

### 有事マインド

戦争が起きた時など生き残ろう、国や家族を守ろうとするサバイバルモード。自分で考えて行動するというより、能力の高い指揮官に管理機能や戦略機能を集約し、他の人は指示に従い最大規模の成果を目指す。

### 平時マインド

平和な時のモード。指揮官が持っていた権限を部下に委譲し、新規企画など新たな取り組みをはじめ、チームやメンバーの成長も重要な成果と捉えるようになる。

## 平時マインドは弱い

平時マインドを会社として保ち続ける事は簡単ではありません。

予算の未達が見えてくると部署は総力を上げて売上を取りにいきます。一部書で足りない場合はさらに会社を上げて売上を取りに行きます。個人的には「ユーザーを刈り取る」なんて言葉が出始めたら有事マインドの前兆かなと思っています。

「売上達成率99%で未達でした。これは残念な事です」という話は経験のある人も多いのではないでしょうか？

しかしながら目標というものは前年度に比べて110%とかに定められている事が多いと思います。採用など事業規模の拡大フェーズなどもあるので一概には言えませんが、ほとんどの場合達成率が80%くらいでも問題なく経営は行えると思います。

しかし誰かが「やばい！このままだと予算未達だ！！」と大きな声を上げると社内は一気に有事マインドに切り替わります。

有事マインドになるとイノベーションや負債整理、メンバー育成などがどんどん後回しになり、次の投資ができません。

## 有事マインドで危機を乗り越えると強い成功体験になってしまう

有事マインドで目標を達成してしまうと、その成功体験が経営メンバーや管理者には強く印象に残ります。

この印象が平時マインドへの移行を難しくさせてしまいます。

売上/利益を最短で得るのであれば有事マインドはとても有用です。倒すべき敵、やるべき事が明確となり、それに短期で直接関係しないものは優先順位を極端に下げてしまいます。

この時、経営者や指揮官が何を考えるでしょうか？

**「売上や利益を伸ばすにはこの方法が最適だ」**

こういった考えになってしまうと簡単には平時マインドにはなりません。自ら新しい目標を立て、常に有事マインドを維持しようとしてしまいます。

これは現代だけでなく、古代ギリシャや中国の歴史を見ても明白です。

## 有事マインドは"想定外"や"新しい事"に弱い

これだけだと有事マインドで進める事にはメリットが多いように感じます。

しかしながら有事マインドにも弱点があります。それは**想定外**や**新しい事に弱い**という事です。

メンバー自体に権限譲渡を行なってこなかったので、自発的に考えて行動する能力が低くなっています。これは管理層にも言える事で、想定外の時の切り替えが遅くなったりできない事も多いです。

> ある会社には主力商品Aとそれほど売れていないBがあったとします。
> しかし全体の売上が芳しくなかったので、全社を上げてAに注力する事にしました。
> するとAの売上が大幅に伸び、その年の予算を達成できました。
> まだ会社としては余力が十分でないため社長はAに注力し、更に伸ばす事にします。
> そして次の年も予算を超える売上を出せ、会社も以前より余力が持てるようになりました。

本来余裕ができたタイミングで平時マインドに切り替えるべきでした。
しかし上手くいっている状況を破壊するには勇気と度胸、胆力が必要です。

> ある年に海外競合の影響で主力商品Aの売上が落ち込みます。
> しかし有事マインドの時は「自分達の努力が足りないからだ」となり、他の選択肢が出てきません。
> 寧ろ他の選択肢の提案は自分達の存在を否定する事だと考える人も多くなり、より有事マインドが強固になっていきます。

ここまで酷い事はないかもしれないですが、結果的に会社として社員から打開策など提案は上がって来ず、社長が全て決めるようになります。

人間一人の案など知れています。ここで出てくる案は社長が新規商品を考えるか、誰も通らない新規企画大会が開かれるくらいでしょう。

## 有事マインドでメンバーが死ぬ

戦争だと前線に出て戦うのは現場のメンバーです。実際の社会では死ぬという事はないですが、長く続くと病んだり退職が続出します。

「辞めたら補充したらいい」から始まって「あれ？人いなくなったけど、売上下がってないじゃん。さては今まで現場は手を抜いていたのか？」みたいな疑心暗鬼も出てきます。

そのメンバーのストレスの捌け口としても**息抜き**は必ず必要です。有事・平時関係なくこれを設計できる人がいる組織が強い事は間違いないです。

ここで言う息抜きというのは「誰も通らない新規企画大会」や「決起集会的な飲み会」の事ではありません。**有事を平時と勘違いさせる**ようなメンバーの負担を和らげるような取り組みのことです。

## 有事マインドと平時マインドのバランスが重要

ありきたりな話になってしまいますが、結局のところ**バランスが重要**という話に落ち着きます。

本当に倒産しそうな有事な時もあります。その時にエンジニアが「自分は予定通りリファクタリングがあるので、突発の開発はしません」と言われたら「ちょっと待て」となるのは明白です。

スタートアップで赤字予算で一つのサービスに注力している中、「新規企画を持ってきました」と言ってくる平時マインドは困ります。

常に将来への投資は惜しむべきではないですが、社内リソースの問題で致し方ない時も出てきます。平時の時から、そういった時の理解を社内に広めておくのも平時にできる準備かもしれません。

会社が潰れるという場面でもないのに一年という期間で見た時に、有事マインドでの動きが多いと感じたら会社としては落ち着いて現状を確認した方が良いでしょう。
