---
title: "Cloud Load Balancingを使用してCloud Storageにサブドメインを設定する"
created: "2023-01-16"
eyecatch: "/images/set-up-subdomain-for-cloud-storage/eyecatch.png"
ogImage: "/images/set-up-subdomain-for-cloud-storage/eyecatch.png"
---

Cloud Storageにサブドメインを設定する際、本来はbucket_nameにサブドメインと同じ名前を設定して、CNAMEに`c.storage.googleapis.com`を設定したら反映されてるというお手軽な方法がありました。

しかし今回は諸事情でCloud Load Balancingを使用してCloud StorageにGoogle Domainsで取得したドメインでサブドメインを設定する事になりました。

Cloud Storageでのバケットの作成、Google Domainsでドメインの取得は省きます。

## Cloud Load Balancingの設定

### ネットワーク管理者権限の付与

はじめにCloud Load Balancingのページにいくとこんな感じなってると思います。

<img src="/images/set-up-subdomain-for-cloud-storage/non-network-maneger-authority.png" width="100%" alt="ネットワーク管理者権限がない時の表示" />

**「申し訳ございません。サーバーはリクエストを実行できませんでした。」**と出ている場合は、`ネットワーク管理者`の権限ないと思うので付与して下さい。

### 証明書の作成

上記の画像の`ロード バランシング コンポーネントのビュー`をクリックします。

その後、[証明書]のタブを押して、[+SSL証明書を作成]をクリックします。

特に事情がなければ、[Google マネージドの証明書を作成する]を選択してください。

あとは、任意の名前をつけて、ドメインに今回使用したいサブドメイン(例: image.〇〇.com)を設定します。

完了したら、作成を押します。

以上で証明書の作成は完了です。

### ロードバランサの作成

ページを戻り、[+ロードバランサを作成]をクリックします。

HTTP(S) ロード バランシングの[構成を開始]をクリックします。

この辺はよくわからなかったので、[インターネットから VM またはサーバーレス サービスへ]と[グローバル HTTP(S) ロードバランサ]を選択しました。

次に各構成について設定していきます。

#### フロントエンドの構成

<img src="/images/set-up-subdomain-for-cloud-storage/frontend.png" width="100%" alt="フロントエンドの設定フォーム" />

適当な名前をつけてます。

プロトコルを[HTTPS(HTTP/２を含む)]にしています。後はデフォルトのままです。

証明書は前述で作成したものを指定して下さい。


#### バックエンドの構成

<img src="/images/set-up-subdomain-for-cloud-storage/backend.png" width="100%" alt="バックエンドの設定フォーム" />

適当なバックエンド バケット名をつけます。

次にCloud Storageのバケットを指定します。

Cloud CDNは必要に応じて、有効/無効を切り替えて下さい。**結構高いので有効にする場合は慎重に...**

#### ルーティングルール

<img src="/images/set-up-subdomain-for-cloud-storage/rule.png" width="100%" alt="ルーティングルールの設定フォーム" />

モードは[単純なホストとパスのルール]を選択します。

あとは先ほど作成したバックエンドを指定します。

## Google Domainsの設定

指定したいドメインのDNSのカスタムレコードにサブドメインを設定します。

<img src="/images/set-up-subdomain-for-cloud-storage/record.png" width="100%" alt="サブドメインの設定" />

`xxx.xxx.xxx.xxx`にはロードバランサのIPが入るので、作成したロードバランサの詳細で確認してください。（ポートは不要）

以上で設定は完了です。

あとは数時間から1日ほど待てばimage.〇〇.comなどのURLでバケットにアクセスできるようになります。

## 料金について

Cloud Load Balancingの料金については、ルールが5つ以下でインバウンド、アウトバウンドが1Gで27.75ドル。同じくルールを増やさずにインバウンド、アウトバウンドが1TBでおおよそ40ドルです。
