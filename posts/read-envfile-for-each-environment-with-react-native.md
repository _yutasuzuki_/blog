---
title: "React Nativeの開発にてXcodeで環境別にENVファイルを読み込む"
created: "2022-04-12"
eyecatch: "/images/read-envfile-for-each-environment-with-react-native/eyecatch.png"
ogImage: "/images/read-envfile-for-each-environment-with-react-native/eyecatch.png"
---

React NativeでiOSのアプリを開発する際にlocal、development、staging、productionなどによってAPIの呼び先などを変えたくなる事があります。
.envを元に設定を変更するような作りにしています。

## ディレクトリ構成

自分の場合は、configフォルダを作ってその中にenvファイルとfirebaseの設定用にGoogleService-Info.infoを環境名を追加した形に変更して保存しています。
どこに設定しても対して問題ないので、好きなところに保存していただければと思います。

```ts
├── App.tsx
├── index.js
├── config
│   ├── GoogleService-Info
│   │   ├── GoogleService-Info-dev.plist
│   │   ├── GoogleService-Info-production.plist
│   │   └── GoogleService-Info-staging.plist
│   └── env
│       ├── .env.dev
│       ├── .env.production
│       └── .env.staging
```

### .env.xxxの設定

ENV_NAMEには環境名を入れます。タイトルの.env.xxxのxxxに対応する名前を入れてください。

```bash
ENV_NAME="dev"
```

## react-native-configとFirebaseを設定

### [react-native-config](https://github.com/luggit/react-native-config)

.envファイルを読み込むmoduleです。最新のReact Nativeを使用していれば`npx pod-install`で使用できます。

### [@react-native-firebase](https://rnfirebase.io/)

Firebaseを使用できるようにするmoduleです。ネイティブのファイルに若干の記述が必要ですが、ドキュメント通りにコピペすれば動きます。

**⚠️ たぶん[GoogleService-Info.plistの読み込み](https://rnfirebase.io/#generating-ios-credentials)はしなくて大丈夫です（試してないので、ダメなら読み込んでください）**

## Xcodeでschemeの設定

メニューから[Product] → [Scheme] → [Edit Scheme...] → [Duplicate Scheme]でデフォルトのSchemeを複製し、devやstagingのようなわかりやすい名前を付けます。

その後は[react-native-configのREADME](https://github.com/luggit/react-native-config#availability-in-build-settings-and-infoplist)のように設定します。

### 自動で.envを読み込むようにする

これでreact-native-configの設定が終わったので、以下のように起動するとenvファイルを読み込んでくれるようになります。

```bash
ENVFILE=.env.dev react-native run-ios           # bash
SET ENVFILE=.env.dev && react-native run-ios    # windows
env:ENVFILE=".env.dev"; react-native run-ios    # powershell
```

ただこれだと実機でビルドする際に少し面倒なので、Xcodeでschemeを選択してRunをするだけでいいようにします。

react-native-configはデフォルトではrootにあるenvファイルを読みにいきます。

なので`cp`コマンドでコピーをroot直下に都度作成します。

```bash
cp "${PROJECT_DIR}/../config/env/.env.dev" "${PROJECT_DIR}/../.env"
```

最終的には以下のような感じの設定になると思います。

<img src="/images/read-envfile-for-each-environment-with-react-native/pre-action.png" width="100%" />

### GoogleService-Info.pilotの読み込み

GoogleService-Info.pilotは利用上はXcode上で参照登録して、.pilotをビルド先にコピーするようになっていると思います。

なのでこれも`cp`を使って強制的に環境に適したものに変更していきます。

[Build Phases]を選択し、[+]で新たに**copy GoogleService-Info**を作成しました。

そこに以下のコマンドを追加していきます。

```bash
SRC_GS_INFO="${PROJECT_DIR}/../config/GoogleService-Info/GoogleService-Info-${ENV_NAME}.plist"
DIST_GS_INFO="${BUILT_PRODUCTS_DIR}/${PRODUCT_NAME}.app/GoogleService-Info.plist"

rm -rf "${DIST_GS_INFO}"
cp -f "${SRC_GS_INFO}" "${DIST_GS_INFO}"
```

自分の失敗としては、**Copy Bundle Resources**にGoogleService-Info.plistが追加されていたので、先にコピーしてもCopy Bundle Resources内の設定で上書きされてしまうという事がありました。その為、順番をCopy Bundle Resourcesの下にしています。

<img src="/images/read-envfile-for-each-environment-with-react-native/build-phases.png" width="100%" />

## 最後に

今回はSchemeを使って出し分けをしていますが、もちろんconfigurationを使っての振り分けもできます。

まだアプリの開発については手探りな部分が多いので、使い分けが必要になったタイミングで追記・修正していきたいと思います。
