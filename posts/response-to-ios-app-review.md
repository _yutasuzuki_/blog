---
title: "iOSアプリのレビューに対しての対応まとめ"
created: "2022-10-04"
eyecatch: "/images/response-to-ios-app-review/eyecatch.png"
ogImage: "/images/response-to-ios-app-review/eyecatch.png"
---

業務用のアプリをリリースするにあたり、当然のように何度かリジェクトをくらいリリースに漕ぎ着けました。

その際に、レビューに対してどういった対応をしていったのかを記述していきたいと思います。

リリース手順に当たっては以下のリンクを参考にしました。
* [（2022年版）iOSアプリをAppStoreで公開する手順まとめ](https://zenn.dev/moutend/articles/feebf0120dce6e6426fa)

翻訳はDeepLを使用しています。

## 却下の理由一覧

### Guidelines 3.1.1 - In-App Purchase and 3.1.3(c) - Enterprise Services

We noticed in our review that your app offers enterprise services that are sold directly to organizations or groups of employees or students. However, these same services are also available to be sold to single users, consumers, or for family use without using in-app purchase.


When an organization or group buys access to your app's enterprise services, you don't need to use in-app purchase. But when providing access to an individual user, consumer, or for family use, you should use in-app purchase.


> [翻訳] 御社のアプリは、組織や従業員・学生のグループに直接販売される企業向けサービスを提供しているとレビューで拝見しました。しかし、これらの同じサービスは、アプリ内課金を使用せずに、1人のユーザー、消費者、または家族用に販売することも可能です。
>
> 組織やグループがあなたのアプリのエンタープライズサービスへのアクセスを購入する場合、アプリ内課金を使用する必要はありません。しかし、個人ユーザー、消費者、または家族のためにアクセスを提供する場合は、アプリ内課金を使用する必要があります。


はじめはアプリ内課金を作らないといけないかと思ったのですが、組織や従業員・学生のグループに直接販売する場合は、個人ユーザーが使えないようにどういった対応をしているかを聞いてるだけでした。

これを見た時はApple Business Managerを使えって事なのかと質問したら「そういう事ではない」と返ってきました。
Appleからの対応策の例に「アプリを修正し、組織や従業員・学生のグループに対してのみ、直接サービスを提供できるようにする。」というのがあり、これを自分はApple Business Managerの事を指すのかと勘違いしてました。

#### 解決策
以下の文章の説明に加えて、図を作りました。（もちろん本来はスクショで作っています）

> To use our application, you need to apply through our website.
>
> 1) At the time of application, the company must submit the corporate number.
>
> 2) We will review it and only approved companies will be able to use our service. Therefore, individual users cannot register.
>
> 3) Employees must belong to the company.
>
> 4) The fee model is based on the number of accounts + basic usage fee. Fees based on the number of accounts will be pro-rated for the contract month and termination month.

<img src="/images/response-to-ios-app-review/registration-flow.jpg" width="100%" />

その後、**Thank you for the message and further information, the 3.1.1 guideline issue has been resolved**という事で無事にレビューをクリアできました。

### Guideline 4.0 - Design

We noticed an issue in your app that contributes to a lower-quality user experience than App Store users expect:

Your app's permissions requests are written in English while the app is set to the Japanese localization. To help users understand why your app is requesting access to a specific feature, your app's permission requests should be in the same language as your app's current localization.

Since App Store users expect apps to be simple, refined, and easy to use, we want to call your attention to this design issue so you can make the appropriate changes.

> [翻訳] 私たちは、App Storeのユーザーが期待するよりも低い品質のユーザー体験に貢献する、あなたのアプリの問題に気づきました。
>
> アプリが日本語にローカライズされているにもかかわらず、アプリのアクセス許可要求が英語で書かれています。アプリが特定の機能へのアクセスを要求している理由をユーザーが理解できるように、アプリの許可要求は、アプリの現在のローカライゼーションと同じ言語で記述されるべきです。
>
> App Store のユーザーは、アプリがシンプルで洗練され、簡単に使えることを期待しているので、このデザインの問題に注意を喚起し、適切な変更を行えるようにしたいと思います。

#### 解決策

これは単純なミスでカメラやマイクの許可を求める言葉が適当な英語になってました。これを日本語にしたのですが、次の**Guideline 5.1.1 - Legal - Privacy - Data Collection and Storage**の問題が発生しました。

### Guideline 5.1.1 - Legal - Privacy - Data Collection and Storage

We noticed that your app requests the user’s consent to access the camera and microphone, but doesn’t sufficiently explain the use of the camera and microphone in the purpose string.

To help users make informed decisions about how their data is used, all permission request alerts need to explain how your app will use the requested information.

> [翻訳] あなたのアプリは、カメラとマイクにアクセスするためにユーザーの同意を要求しますが、目的の文字列でカメラとマイクの使用について十分に説明されていないことに気づきました。
>
> ユーザーが自分のデータの使用方法について十分な情報を得た上で意思決定できるように、すべての許可要求アラートでは、要求された情報をアプリでどのように使用するかを説明する必要があります。

#### 解決策
「カメラの許可をお願いします」とだけ記述したのですが、これだと何のためにカメラを使用するのかわからないというガイドライン違反だったみたいです。

ちゃんとユーザーにカメラが何のために使用されるか認識して貰ってから許可が必要だという事だったので、今回は「カメラの許可する事で動画の作業記録が可能になります。」と記述し解決しました。

## まとめ

課金モデルの説明をするところが上手くできるか肝を冷やしましたが、Appleのレビューがとても丁寧だったので解決する事ができました。

各レビューで引っかかった際も、以下のようにヒントをくれるのですんなり解決できました。

> Next Steps
>
> Please revise the purpose string in your app’s Info.plist file for the camera to explain why your app needs access.
>
> You can modify your app's Info.plist file using the property list editor in Xcode.

レビューの担当者が本当に丁寧だったので助かりました。感謝しかないです。
