---
title: "文化資本について"
created: "2024-02-19"
eyecatch: "/images/cultural-capital/eyecatch.png"
ogImage: "/images/cultural-capital/eyecatch.png"
---

**文化資本（cultural capital）** という言葉は、フランスの社会学者ピエール・ブルデューが提唱した言葉で、文化的素養についての個人的な資産を指す言葉です。

人材育成について調べているタイミングで、たまたまPodcastでその言葉を聞き興味を持ち少しだけ調べてみました。しかしながら本や論文などは一切見ておらず、なんとなくの浅い感覚で書いています。

## 文化資本の形態

ブルデューは、文化資本の概念を下記の3つだと言っています。（Wikipedia参照）

### 客体化された形態の文化資本

物質的に所有される譲渡可能な資本。

* 音楽家の家には数多くのCDやレコードがあり、楽器、楽譜、防音室など音楽に容易に触れれる環境が整っている。
* 政治家の家には多くの政治家が訪れ、自身をバックアップしてくれる関係性を築いてる。

### 制度化された形態の文化資本

社会からの公的な承認を得た資本。学歴や資格なども含まれる。

* 貴族などの特権は人ではなく家柄や血筋が保有する。
* 車の免許を持っているから、車の運転をすることができる。

### 身体化された形態の文化資本

個人に身体化されて蓄積された資本。言語、教養、素養、技術、感性などがある。

* よく挨拶をする親を持つ子供はしっかり挨拶できる。
* 習い事を多く習えた人は多くの技術や感性などを得ることができる。

**客体化された形態の文化資本**は経済的な資本と似ているが、それが**制度化された形態の文化資本**や**身体化された形態の文化資本**と直接的に紐付く必要がある。

## 文化資本は親から子へ強く影響する

人材育成で調べていたのですが、文化資本を調べると子育ての話が多かったので、その視点で書きます。

文化資本は親から子供への文化継承とも捉えることができ、それはモノ以外でも所作や振る舞いなど多岐に渡ります。体感的に社長の子供が起業しやすかったりするのは、金銭的な面以外でもこういう文化があるからではないかと思います。

文化資本は単純な金銭的なものを超えた価値があり、教育、技術、知識、生活様式など社会的地位や成功に大きく貢献しています。しかし文化資本の積み上げとその社会的地位への影響は決して固定的なものではなく、教育や社会的な支援を受け文化資本を獲得し、社会的地位を向上する可能性は大いにあります。

以前「金持ちは金持ちの言葉で会話をする」「貧乏人は貧乏人の言葉で会話をする」という言葉を聞いた時は気持ち悪いなと思っていましたが、それは文化資本の交流と捉えると理解できます。

そういった日常の会話からも大人は外部から文化資本を取り入れることができます。しかしながら子供はそのリソースの多くを親からの継承に依存しているものと思われます。

## 子供の学習も同じ

おそらくどんな親も子供に勉強ができた方がいいと思っています。

しかしながら勉強してこなかった親に勉強のための文化資本はあるのでしょうか？家庭教師や塾などはありますが、資本はあくまで積み上げです。

本を読む親の子供は本を読み、スポーツ選手の子供は何らかのスポーツをやっているケースが多いです。これは才能ではなく幼少期に文化資本をどの程度継承できるかが大切だと言う事を示唆していると思います。

結局、子供が育つ環境は親が作るしかなく、それは物理的なことだけではなく、普段の行いから見直さないといけないということです。

勉強のドリルを買ってもやらないのは、親が自主的に勉強している姿勢を見せたことがなく、勉強することが特殊なことだと子供が認識してしまっている可能性があります。

## 継承元の振る舞い

身も蓋もない話になってしまいますが、子供をエリートにしたかったら、自分がエリートになるということです。音楽家にしたかったら、自分が音楽家になることです。医者にしたかったら自分が医者になることです。

...と言ってもそれはいくらなんでもハードモードです。

体力や金銭的にできる/できないは置いておいて、まずは親が継承できる資本を増やすことが大切だと思います。

提唱された当時とは違いインターネットが普及したことで、情報としての文化資本のハードルは下がっており、金銭的格差も当時よりも縮まっている（中間層が増えている）と思います。

そうなってくるとより大切になってくるものは**身体化された形態の文化資本**です。

所作、振る舞い、学習の姿勢、社会との付き合い方、多角的な物の見方など、親自身の成長が子供の成長に大きく寄与するため、日常的に見直し実践する努力は必要そうです。

## まとめ

文化資本の後に**経済資本**や**社会関係資本**も出てきましたが、今回は敢えて触れずにベースになった文化資本の考え方を雑にまとめました。

自分が言いたいことは「全て揃えないと育成できない」という事ではなく、**「文化資本を継承/獲得するプロセスは日常生活の中から成り立っているので、親であれば自分の努力次第で揃えるチャンスはある」** ということです。

本来「会社の人材育成の文化を作るにはこういう要素があって、こういう振る舞いや環境整備が大切だよー」という話をしたかったのですが、調べて例を考えていくと子育ての話になってしまいました。でも普通の社会人ならきっと抽象化して、人材育成に当てはめれば言いたいことはわかってくれると思ってます。
