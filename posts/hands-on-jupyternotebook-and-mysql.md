---
title: "Jupyter NotebookとMySQLの開発環境を構築しよう"
created: "2023-06-01"
eyecatch: "/images/hands-on-jupyternotebook-and-mysql/eyecatch.png"
ogImage: "/images/hands-on-jupyternotebook-and-mysql/eyecatch.png"
---

## 共通アプリをインストール

### VSCodeインストール

現在最も人気のあるコードエディターです。プラグインを入れることで機能を拡張できます。

[Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)

### TablePlusインストール

最近エンジニア界隈で人気のあるデータベースクライアントアプリです。

[TablePlus | Modern, Native Tool for Database Management](https://tableplus.com/)

## Windows編

### WSL2の設定

WSLはWindows Subsystem for Linuxの略でWindows上でLinuxを操作できます。

1. 右クリックからターミナルを**管理者として実行**から実行。
2. WSLをインストール。  `wsl --install` を実行。
3. PCを再起動。
4. アプリに**Ubuntu**がインストールされているので起動。

### MySQLをインストール

MySQLは人気のあるデータベースです。データベースにはいくつか種類がありますが、今回は一般的なリレーショナル型を採用します。リレーショナル型は他にはPostgreSQL、Oracle Databaseなどがあります。

1. Ubuntu内でapt（Advanced Package Tool）を最新にアップデート。 `sudo apt update` を実行。
2. MySQLをインストール。`sudo apt-get install mysql-server` を実行。
3. MySQLのバージョンを確認。`mysql --version` を実行。
4. MySQLを起動。 `sudo /etc/init.d/mysql start` を実行。
5. MySQLにログイン。 `sudo mysql` を実行。
6. MySQLの管理ユーザー（root）のパスワードをpasswordに変更。 `ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by 'password';` を実行。
7. MySQLからログアウト。 `exit;`を実行。
8. 今度はパスワードを用いてMySQLにログイン。 `mysql -uroot -p`を実行。パスワードの入力を求められたら`password`と入力。
9. ログインできたら再度ログアウト。 `exit;`を実行。

### Pythonの設定

PythonはJupyter Notebookを使用するために必要です。

1. バージョンの確認。`python3 --version` を実行。
2. パッケージ管理ツールpipをインストール。  `sudo apt install python3-pip` を実行。
3. 仮想環境を作るvenvをインストール。  `sudo apt install python3-venv` を実行。

### Jupyter Notebook

Pythonを実行できるエディター環境です。

1. Anacondaをダウンロード。 `wget https://repo.anaconda.com/archive/Anaconda3-2023.03-1-Linux-x86_64.sh` を実行。
2. Anacondaをインストール。 `bash Anaconda3-2023.03-1-Linux-x86_64.sh` を実行。
3. [Linux] → [Ubuntu] → [home] → [name] → .bashrc（右クリックからVSCodeで開く）
4. `export PATH=~/anaconda3/bin:$PATH` を一番下に追加する。
5. ターミナルで`sourse .bashrc`を実行。
6. Jupyter Notebookをインストール。`sudo apt install jupyter-notebook` を実行。
7. Jupyter Notebookを起動。 `jupyter notebook` を実行。
8. Jupyter Notebookの終了。 `crtl + c`を連打。

### Jupyter Notebookのカスタマイズ

余力があればやった方がファイルが散らかりにくいです。

1. [Linux] → [Ubuntu] → [home] → [name] に移動。
2. `jupyter`フォルダを作成。
3. [Linux] → [Ubuntu] → [home] → [name] → [.jupyter]→ jupyter_notebook_config（右クリックからVSCodeで開く）
4. `#c.NotebookApp.notebook_dir=''`の#を外して、`''`を`'/home/{name}/jupyter'`に置き換える。`{name}`は自分のフォルダの名前。

## Mac編

### Homebrewをインストール

パッケージ管理ツールです。いろいろなプログラムやアプリをコマンドからインストールする事ができます。

1. ターミナルを開く。
2. ターミナルで `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"` を実行。
3. Homebrewを最新にアップデート。`brew update` を実行。

### MySQL

1. MySQLをインストール。`brew install mysql` を実行。
2. MySQLのバージョンを確認。`mysql --version` を実行。
3. MySQLを起動。 `mysql.server start` を実行。
4. MySQLにログイン。 `sudo mysql` を実行。
5. MySQLの管理ユーザー（root）のパスワードをpasswordに変更。 `ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by 'password';` を実行。
6. MySQLからログアウト。 `exit;`を実行。
7. 今度はパスワードを用いてMySQLにログイン。 `mysql -uroot -p`を実行。パスワードの入力を求められたら`password`と入力。
8. ログインできたら再度ログアウト。 `exit;`を実行。

### Pythonの設定

PythonはJupyter Notebookを使用するために必要です。

1. バージョンの確認。`python3 --version` を実行。

### Jupyter Notebook

1. jupyterをインストール。`pip3 install jupyter` を実行。
2. Jupyter Notebookを起動。 `jupyter notebook` を実行。
3. Jupyter Notebookの終了。 `crtl + c`を連打。

### Jupyter Notebookの実行時にやると便利なこと

1. ドキュメントに`jupyter`フォルダを作成。
2. ターミナルで`cd ~/Documents/jupyter/` を実行。
3. Jupyter Notebookを起動。 `jupyter notebook` を実行。
