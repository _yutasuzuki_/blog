---
title: "ラーの鏡を作りました"
created: "2022-12-22"
eyecatch: "/images/ras-mirror/eyecatch.png"
ogImage: "/images/ras-mirror/eyecatch.png"
---

> **この記事はクソアプリ Advent Calendar 2022 22日目の記事です。**

ラーの鏡と言えばドラゴンクエストで、主にモシャスで変身したものを真実の姿を映し出すアイテムです。

世の中にはそんなモシャスを使用する不快なモンスターなど登場しませんが、時々そんなモンスターに匹敵するレスポンスが返ってきたりします。

## 思いついた経緯

あるAPIを利用した時に、`"false"`なるものが返ってきました。`"false"`は文字列で存在しており、判定では常にtrueの状態です。
しかしながらこれは自分がparseする際にサンプルをコピペして`toString()`をしていたのですぐに解決したのですが、同時に過去の悲しい思い出が蘇りました。

以前勤めていた会社では当時で既に10年以上運用されてたサービスがありました。

そこで業務をしている中、こんな感じのレスポンスが返ってきました。

```json
{
  "status": true,
  "result": {
    "name": "null",
    "age": "32"
  }
}
```

当然レスポンスの`result.name`を判定すると常にtrueになります。その後バックエンドエンジニアに確認して貰ったところDBに既に文字列で格納されてるという事で、経緯も分からず2人で頭を抱えました。

結局、`result.name === "" || result.name === "null"`という奇妙な処理を入れて事なき(?)を得ました。

しかしながら本来なら`!result.name`で済んでいます。それを思い出し、ラーの鏡（ras-mirror.js）を作ることにしました。

## ラーの鏡の使い方

### インストール

```sh
npm i ras-mirror
```
#### Github
[https://github.com/yutasuzuki/ras-mirror](https://github.com/yutasuzuki/ras-mirror)

#### NPM
[https://www.npmjs.com/package/ras-mirror](https://www.npmjs.com/package/ras-mirror)

### 基本的な使い方

文字列化された値をラーの鏡に渡すと変換されます。

```ts
// boolean
rasMirror<boolean>("true") // true

// Date
rasMirror<Date>("2022-12-31 12:34:10") // 2022-12-31T03:34:10.000Z

// Number
rasMirror<number>("12.345") // 12.345

// String
rasMirror("foo") // "foo"

// null
rasMirror("null") // null

// undefined
rasMirror("undefined") // undefined

// NaN
rasMirror("NaN") // NaN
```

### Array

```ts
const stringArray = [
  {
    "id": 1,
    "name": "foo"
  },
  {
    "id": "2",
    "name": "bar"
  }
]

type ArrayType = {
  id: number
  name: string
}[]

const result = rasMirror<ArrayType>(stringArray)
/* [ { id: 1, name: 'foo' }, { id: 2, name: 'bar' } ] */
```

### Object

```ts
type jsonType = {
  result: boolean
  count: number
  items: itemType[]
}

type itemType = {
  id: number
  name: string
  create_at: Date
  delete_at?: Date 
}

const stringJson = { 
  "result": "false",
  "count": "2",
  "items": [
    {
      "id": "1",
      "name": "foo",
      "create_at": "2022/12/31 12:34:50",
      "delete_at": "undefined"
    },
    {
      "id": "2",
      "name": "bar",
      "create_at": "2023-1-1 1:23:45",
      "delete_at": "undefined"
    }
  ]
}

const result = rasMirror<jsonType>(stringJson)
/* {
  result: false,
  count: 2,
  items: [
    {
      id: 1,
      name: 'foo',
      create_at: 2022-12-31T03:34:50.000Z,
      delete_at: undefined
    },
    {
      id: 2,
      name: 'bar',
      create_at: 2022-12-31T16:23:45.000Z,
      delete_at: undefined
    }
  ]
} */

```

## 開発について

実はテストを書いた事が殆どありませんでした。

そのため、今回はmoduleが`toBoolean.ts`や`toDate.ts`など分けやすい事もあったので、開発に当たってテストを書くという目的も出来ました。

### なんちゃってテスト駆動

先にテストを書いて、それをクリアできるように開発しました。テストはJestを利用しました。

しかしながら**どこまでテストしたらいいかわからん状態**になりました。これはコツや勘所の話なのか、ある程度の業界標準があるのかわかりませんがみんなどこまでテストしてるんでしょうか？

とりあえずはカバレッジを埋める事を目標にしました。

テスト自体はとてもシンプルなので、入門するのにはちょうど良かったかなと思ってます。

## 実用性について

**実用性はありません。** これを使うくらいならAPIやDBを見直してください。

## 今後について

**今後開発を続ける予定はありません。**

## 最後に

クソアプリではなくクソモジュールになってしまったのですが、書く場所もなかったのでこちらに書かせて頂きました。

npmで実際にダウンロードして使ってないので動かなかったら申し訳ありません。
