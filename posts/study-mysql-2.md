---
title: "MySQLで条件を指定して値を取得する"
created: "2023-06-22"
eyecatch: "/images/study-mysql/eyecatch.jpg"
ogImage: "/images/study-mysql/eyecatch.jpg"
---

[前回](https://0ops.dev/posts/study-mysql-1)からの続きで行っていきます。

## userに誕生日を追加する
※TEMPORARYを使うことは特殊なのでマーケとかの人は覚えなくても大丈夫です。


#### 1. userにbirthdayカラムをDATE(日付型)で追加（nameの後ろ）
```sql
ALTER TABLE user 
  ADD birthday DATE 
  AFTER name; 
```

#### 2. TEMPORARYという一時的なtemp_user_birthdayテーブルを作る
```sql
CREATE TEMPORARY 
  TABLE temp_user_birthday
    (id INT, birthday Date);
```

#### 3. temp_user_birthdayにデータを入れる
```sql
INSERT INTO temp_user_birthday 
    (id, birthday)
  VALUES 
    (1, '1980-08-07'),
    (2, '1999-12-21'),
    (3, '1992-03-17'),
    (4, '2006-10-28');
```

#### 4. temp_user_birthdayのidと紐づくユーザーにtemp_user_birthdayに入力した誕生日を入れる
```sql
UPDATE user
  JOIN 
    temp_user_birthday
  ON 
    user.id = temp_user_birthday.id
  SET 
    user.birthday = temp_user_birthday.birthday;
```

#### 5. もう一人ユーザーを追加
```sql
INSERT INTO user
    (name, birthday, address)
  VALUE
    ('渡辺', '2001-07-05', '愛媛県');
```

## 比較演算子

比較演算子は`>`,`<`,`>=`, `<=`を指します。

#### birthdayが1995-01-01より大きいuserを抽出

```sql
SELECT * from user
  WHERE
    birthday > '1995-01-01';
```

#### 1999-12-21より大きいという意味なので1999-12-21の人は表示されない
```sql
SELECT * from user
  WHERE
    birthday > '1999-12-21';
```

#### =をつけることで1999-12-21生まれの人も含まれる
```sql
SELECT * from user
  WHERE
    birthday >= '1999-12-21';
```

## 期間で取得

期間はBETWEENを使います。

#### 90年代の人だけ取得
```sql
SELECT * from user
  WHERE
    birthday
      BETWEEN '1990-01-01' AND '1999-12-31';
```

#### 同一の日は含まれる
```sql
SELECT * from user
  WHERE
    birthday
      BETWEEN '1992-03-17' AND '1999-12-21';
```
#### 90年代以外の人を取得
```sql
SELECT * from user
  WHERE
    birthday
      NOT BETWEEN '1990-01-01' AND '1999-12-31';
```

## 複数の条件

複数の条件は`AND`, `OR`などで表現します。

#### 住所が北海道で90年代生まれの人を取得
```sql
SELECT * FROM user 
  WHERE
    address = '北海道' 
  AND
    birthday
      BETWEEN '1990-01-01' AND '1999-12-31';
```

#### 住所が愛知県、もしくは90年代生まれの人を取得
```sql
SELECT * FROM user 
  WHERE
    address = '愛知県' 
  OR
    birthday
      BETWEEN '1990-01-01' AND '1999-12-31';
```

## 同じカラムから複数条件で取得

同じカラムからの複数条件はINを使用します。

#### 愛知県と東京都のみ取得
```sql
SELECT * FROM user
  WHERE
    address IN ("愛知県", "東京都");
```
#### 愛知県と東京都以外を取得
```sql
SELECT * FROM user
  WHERE
    address NOT IN ("愛知県", "東京都");
```

## 曖昧検索

文字列の曖昧検索はLIKEと`%`を組み合わせて使用します。

#### 前方検索
```sql
SELECT * FROM user
  WHERE
    address LIKE "大%";
```
#### 後方検索
```sql
SELECT * FROM user
  WHERE
    address LIKE "%県";
```
#### 中間検索
```sql
SELECT * FROM user
  WHERE
    address LIKE "%海%";
```
#### 前後方検索
```sql
SELECT * FROM user
  WHERE
    address LIKE "愛%県";
```
#### 愛%県以外を取得
```sql
SELECT * FROM user
  WHERE
    address NOT LIKE "愛%県";
```

## 数を取得
#### 単純に数えるだけ
```sql
SELECT COUNT(user_id) FROM user_post;
```
#### user_idの重複を無視
```sql
SELECT COUNT(DISTINCT user_id) FROM user_post;
```

## データをグループ化する

GROUP BYとCOUNTを組み合わせて使用します。

#### GROUP BYで数えたいカラムを取得して、COUNTで数を表示
```SQL
SELECT user_id, COUNT(user_id) as '投稿数' FROM user_post
  GROUP BY 
    user_id;
```

## グループ化したデータを比較する

単純な比較や条件はWHEREを使って行ってきましたが、GROUP BYしたものに関してはHAVINGを使って絞り込みをしていきます。

#### HAVINGを使って投稿数が2件以上のユーザーを取得 
```sql
SELECT user_id, COUNT(user_id) as '投稿数' FROM user_post
  GROUP BY 
    user_post.user_id
  HAVING
    COUNT(user_id) >= 2;
```

## 問題

投稿数が2件以上のユーザーID, 投稿者、投稿数を表示する

```sql
SELECT user.id as 'ユーザーID', user.name as '投稿者',COUNT(user_id) as '投稿数' FROM user_post
  JOIN user
    ON user.id = user_post.user_id
  GROUP BY 
    user_post.user_id
  HAVING
    COUNT(user_id) >= 2;
```


