---
title: "アベレージ90のゴルフチップス"
created: "2023-05-10"
eyecatch: "/images/golf-tips-with-ave-90/eyecatch.jpg"
ogImage: "/images/golf-tips-with-ave-90/eyecatch.jpg"
---

ゴルフをはじめて約1年100切りが出来ませんでしたが、初の100切りが91で2年目を目前に90切りもでき、最近は90前後でスコアが安定するようになりました。

練習も勿論しましたが、マネジメントによる影響が大きく、自分のマネジメントに反することを100叩く事もあります。（とはいえそれはそれで楽しい）

## メンタル面

### ミスではないように振る舞う

当然ミスショットを連発しますが、自分の中で「はじめからあのミスは想定済みです」みたいな振る舞いをしてます。OBや池はキツいですが「1発は許容範囲です」「池だけど前から打てるからラッキー」くらいに振る舞うとメンタルが安定します。

* トップやダフったけど、バンカーに入らず100yd転がったからOK。
* バンカーだけどOBじゃないからOK。
* アプローチが寄らなかったけど、グリーンに乗ったからOK。
* 3パットだけど打ちたい方向に打ててるからOK。

上記くらい適当によかったところを見つけてOKとしてます。

### ダブルボギーを許容する

1ホール目とかは特にダブルボギーで良いくらいに思っています。そもそも自分はゴルフコースに行って、よほど時間に余裕がないと朝練習はしません。
なので1ホール目のティーショットとかはOBじゃなければチョロでもOKだと思っています。

他にもアンジュレーションがキツいグリーン、長いPar4などもダブルボギーで良いです。

### 1メートル以上のパターは外れて当然

藤田寛之プロの1.2メートルのパターでプロでも入る可能性が70%、1.5メートルだと30%ということです。

[えーっ、1.5メートルのパットがカップインする確率は30％！？ | Gridge](https://gridge.info/article/beginner/detail.php?id=4797)

ショートゲーム、パターの名手の藤田プロがこのスタッツなので外して当然ですし、狙いすぎて大きなミスにするより2パットで上がることを考えています。

## 戦略面

### メインクラブを決める

自分の中で得意なクラブを作ります。練習も9割そのクラブしかしません。

クラブ同士の距離感覚で決めるのではなく「適当に降っても当たってくれる」みたいな信頼感の高いクラブが良いです。

今の自分は以下がメインクラブです。

* 7W
* 7I
* PW

最近5Uを買い、こちらもかなり調子がいいので、こちらもメインクラブ入りする可能性が高いです。

### パーオンではなく、ボギーオンを狙う

パーオンしたら確かにゴルフは楽ですし、それを狙うのも醍醐味だと思います。ただグリーンの周りにはバンカーなどのトラップも多いので、無理してパーオンを狙う必要はないと思っています。

自分は90でまわれた時にパーオン率0%ということもありました。アプローチで寄せればたまにパーが取れます。

### グリーンを狙う時は広い場所

どうしてもピンを狙ってしまいがちですが、ピンというのは隅に切ってある事が多いです。狙いすぎてグリーンを外して、バンカーやラフに入るくらいなら真ん中に乗せた方がパター2回で上がれる可能性が高いです。

具体的にいうとピンが奥でも手前でも、グリーンの中心から10yd手前くらいのキャリーを狙ってます。

### アプローチの順番を決める

アプローチの考え方で常に以下の順番を意識します。

1. パター
2. PW
3. SW

ファーストチョイスは常にパターです。パターはトップやダフるリスクはありません。

2のPWはダフる可能性はありますが、トップしても意外と距離感は変わりません。バックスピンも入りにくいので慣れてくると転がる距離も安定します。あとグリーンのラインが見えるので好んで使っています。

3のSWはピンが手前で転がしたくない時などに使います。距離があるときにSWを使うと思った以上にきれいに打てたせいで、スピンが入って途中で止まってしまうこともあるので、サードチョイスです。


## まとめ

ここに書いてある内容は、いろいろなメディアで散々叩かれた内容です。ただ実際に実践して、自分の腹落ちしたものだったのでまとめました。

恐らくもっと高度なAve.80のマネジメントや片手シングルの世界もあると思いますが、今はこの考え方でスコアが安定しているので当分はこれでいこうと思っています。
